<?php

namespace app\controllers;

use yii\rest\ActiveController;
use app\models\Flag;
use app\models\Device;
use app\models\Event;
use yii\db\Query;
use Yii;

class RestController extends ActiveController {

    public $modelClass = 'app\models\Flag';

    public function actionTest($id) {
        $model = new Flag();
        return $model->findAll(['id' => $id]);
    }

    public function actionDeviceList($q = null, $id = null) {
        $out = ['results' => ['id' => '', 'text' => '']];
        if (!is_null($q)) {
            $query = new Query;
            $query->select('id, sn AS text')
                    ->from('device')
                    ->where(['like', 'sn', $q])
                    ->limit(20);
            $command = $query->createCommand();
            $data = $command->queryAll();
            $out['results'] = array_values($data);
        } elseif ($id > 0) {
            $out['results'] = ['id' => $id, 'text' => Device::find($id)->name];
        }
        return $out;
    }

    public function actionSubcat() {
        $deviceId = \Yii::$app->request->Post()['depdrop_parents'];

        $lastEvent = Event::find()->where(['deviceId' => $deviceId])->orderBy('created DESC')->one();
        if ($lastEvent) {
            $flagList = $lastEvent->flag->children;
            if ($flagList) {
                return ['output' => $flagList];
            } else {
                return ['output' => [['id' => 'null', 'name' => 'Urządzenie jest na końcowym etapie. Nie możesz dodać nowego zdarzenia.']]];
            }
        } else {
            $flagModel = new Flag();
            return ['output' => $flagModel->HeadFlags];
        }
    }

    public function actionAddEvent($sn, $flagName) {
        $flag = Flag::find()->where(['name' => $flagName])->one();
        if (!$flag) {
            return ['message' => 'Podana flaga nie istnieje.'];
        }

        $device = Device::find()->where(['sn' => $sn])->one();
        if (!$device) {
            $device = new Device;
            $device->sn = $sn;
            $device->save();
        }

        //Jeżeli urządzenie nie posiada flag sprawdzamy czy użytkownik podał jedną z początkowych flag
        if (!$device->events) {
            $flagModel = new Flag();
            $headFlags = $flagModel->HeadFlags;

            $exist = false;
            foreach ($headFlags as $key => $value) {
                if ($flagName == $value['name']) {
                    $exist = true;
                }
            }

            if (!$exist) {
                return ['message' => 'Podana flaga nie może być przypisana. Urządzenie znajduje się na etapie, który na to nie pozwala.'];
            } else {
                $eventModel = new Event();
                $eventModel->deviceId = $device->id;
                $eventModel->flagId = $flag->id;
                $eventModel->ip = Yii::$app->request->userIP;
                $eventModel->save();

                //Aktualizacja daty modyfikacji urządzenia
                $device = Device::find()->where(['id' => $device->id])->one();
                $device->modified = null;
                $device->save();

                return $eventModel;
            }
            //Urządzenie posiada już inne flagi
        } else {
            $lastEvent = Event::find()->where(['deviceId' => $device->id])->orderBy('created DESC')->one();
            $flagList = $lastEvent->flag->children;
            if (!$flagList) {
                return ['message' => 'Podana flaga nie może być przypisana. Urządzenie znajduje się na etapie, który na to nie pozwala.'];
            } else {
                $exist = false;
                foreach ($flagList as $key => $value) {
                    if ($flagName == $value['name']) {
                        $exist = true;
                    }
                }
                if (!$exist) {
                    return ['message' => 'Podana flaga nie może być przypisana. Urządzenie znajduje się na etapie, który na to nie pozwala.'];
                } else {
                    $eventModel = new Event();
                    $eventModel->deviceId = $device->id;
                    $eventModel->flagId = $flag->id;
                    $eventModel->ip = Yii::$app->request->userIP;
                    $eventModel->save();

                    //Aktualizacja daty modyfikacji urządzenia
                    $device = Device::find()->where(['id' => $device->id])->one();
                    $device->modified = null;
                    $device->save();

                    return $eventModel;
                }
            }
        }
    }

}
