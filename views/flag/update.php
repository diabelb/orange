<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Flag */

$this->title = 'Zmień flagę: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Flags', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="box">
    <div class="box-header with-border">
    </div><!-- /.box-header -->
    <div class="box-body">
        <div class="flag-update">
            <?=
            $this->render('_form', [
                'model' => $model,
            ])
            ?>

        </div>
    </div>
</div>
