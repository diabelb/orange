<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Flag */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="flag-form">

    <?php 
    $model->ip = Yii::$app->request->userIP;
    $form = ActiveForm::begin(); 
    
    ?>

    <?= $form->field($model, 'name')->textInput() ?>

    <?= ''//$form->field($model, 'ip')->hiddenInput() ?>

    <?= ''//$form->field($model, 'date')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Dodaj' : 'Aktualizuj', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
