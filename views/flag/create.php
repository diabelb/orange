<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Flag */

$this->title = 'Dodaj flagę';
$this->params['breadcrumbs'][] = ['label' => 'Flags', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box">
    <div class="box-header with-border">
    </div><!-- /.box-header -->
    <div class="box-body">
        <div class="flag-create">

            <?=
            $this->render('_form', [
                'model' => $model,
            ])
            ?>

        </div>
    </div>
</div>

