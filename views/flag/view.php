<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\Flag */

$this->title = 'Podgląd: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Flags', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title"></h3>
        <div class="box-tools pull-right">
            <?= Html::a('Zmień', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?=
            Html::a('Usuń', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ])
            ?>
        </div><!-- /.box-tools -->
    </div><!-- /.box-header -->
    <div class="box-body">
        <div class="flag-view">

            <?=
            DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'id',
                    'name:ntext',
                    'ip:ntext',
                    'date',
                ],
            ])
            ?>

        </div>
    </div>
</div>

<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">Rodzice</h3>
    </div><!-- /.box-header -->
    <div class="box-body">
        <div class="flag-view">
            <?=
            GridView::widget([
                'dataProvider' => $parentsDataProvider,
                'columns' => [
                    'id',
                    'name:ntext',
                    'ip:ntext',
                    'date',
                ],
            ])
            ?>

        </div>
    </div>
</div>

<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">Dzieci</h3>
    </div><!-- /.box-header -->
    <div class="box-body">
        <div class="flag-view">
            <?=
            GridView::widget([
                'dataProvider' => $childrenDataProvider,
                'columns' => [
                    'id',
                    'name:ntext',
                    'ip:ntext',
                    'date',
                ],
            ])
            ?>

        </div>
    </div>
</div>