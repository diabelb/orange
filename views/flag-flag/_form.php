<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Flag;

/* @var $this yii\web\View */
/* @var $model app\models\FlagFlag */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="flag-flag-form">

    <?php
    $form = ActiveForm::begin();
    $flagModel = new Flag();
    $flagList = $flagModel->find()->select(['name', 'id'])->indexBy('id')->column();
    ?>
    <?= $form->field($model, 'parentId')->dropDownList($flagList, ['prompt' => 'Wybierz flagę']); ?>
    <?= $form->field($model, 'childId')->dropDownList($flagList, ['prompt' => 'Wybierz flagę']); ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Dodaj' : 'Zapisz', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
