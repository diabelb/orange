<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\FlagFlag */

$this->title = 'Zmień relację';
$this->params['breadcrumbs'][] = ['label' => 'Flag Flags', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="box">
    <div class="box-header with-border">
    </div><!-- /.box-header -->
    <div class="box-body">
        <div class="flag-flag-update">

            <?=
            $this->render('_form', [
                'model' => $model,
            ])
            ?>

        </div>
    </div>
</div>