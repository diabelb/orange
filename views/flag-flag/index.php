<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel app\models\FlagFlagSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Relacje';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="flag-flag-index">
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Lista relacji</h3>
            <div class="box-tools pull-right">
                <?= Html::a('Dodaj relację', ['create'], ['class' => 'btn btn-success']) ?>
            </div><!-- /.box-tools -->
        </div><!-- /.box-header -->
        <div class="box-body">
            <?=
            GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                    [
                        'attribute' => 'parentId',
                        'label' => 'Rodzic',
                        'value' => 'parent.name',
                        'filter' => ArrayHelper::map(app\models\Flag::find()->asArray()->all(), 'id', 'name'),
                    ],
                    [
                        'attribute' => 'childId',
                        'label' => 'Dziecko',
                        'value' => 'child.name',
                        'filter' => ArrayHelper::map(app\models\Flag::find()->asArray()->all(), 'id', 'name'),
                    ],
                    ['class' => 'yii\grid\ActionColumn'],
                ],
            ]);
            ?>
        </div>
    </div>
</div>
