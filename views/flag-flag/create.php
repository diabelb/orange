<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\FlagFlag */

$this->title = 'Utwórz relację';
$this->params['breadcrumbs'][] = ['label' => 'Flag Flags', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box">
    <div class="box-header with-border">
    </div><!-- /.box-header -->
    <div class="box-body">
        <div class="flag-flag-create">

            <?=
            $this->render('_form', [
                'model' => $model,
            ])
            ?>

        </div>
    </div>
</div>
