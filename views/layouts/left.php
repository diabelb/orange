<aside class="main-sidebar">

    <section class="sidebar">

        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu'],
                'items' => [
                    ['label' => 'Menu', 'options' => ['class' => 'header']],
                    ['label' => 'Flagi', 'url' => ['flag/index']],
                    ['label' => 'Relacje', 'url' => ['flag-flag/index']],
                    ['label' => 'Urządzenia', 'url' => ['device/index']],
                    ['label' => 'Zdarzenia', 'url' => ['event/index']],
                ],
            ]
        ) ?>

    </section>

</aside>
