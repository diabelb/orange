<?php

use yii\helpers\Html;
//use yii\widgets\ActiveForm;
use kartik\widgets\ActiveForm;
use app\models\Flag;
use kartik\widgets\Select2; // or kartik\select2\Select2
use yii\web\JsExpression;
use yii\helpers\Url;
use kartik\widgets\DepDrop;

/* @var $this yii\web\View */
/* @var $model app\models\Event */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="event-form">

    <?php
    $form = ActiveForm::begin();
    $flagModel = new Flag();
    $flagList = $flagModel->find()->select(['name', 'id'])->indexBy('id')->column();
    ?>

    <?php
    echo $form->field($model, 'deviceId')->widget(Select2::classname(), [
        'options' => ['placeholder' => 'Wpisz numer seryjny, aby wyszukać urządzenie ...'],
        'pluginOptions' => [
            'allowClear' => true,
            'minimumInputLength' => 1,
            'ajax' => [
                'url' => Url::to(['rest/device-list']),
                'dataType' => 'json',
                'data' => new JsExpression('function(params) { return {q:params.term}; }')
            ],
        ],
    ]);
    ?>

    <?=
        $form->field($model, 'flagId')->widget(DepDrop::className(), [
            'type' => DepDrop::TYPE_SELECT2,
            'pluginOptions' => [
                'depends' => [Html::getInputId($model, 'deviceId')],
                'initialize' => isset($model->flagId) ? true : false,
                'initDepends' => [Html::getInputId($model, 'deviceId')],
                'placeholder' => 'Wybierz',
                'url' => Url::to(['rest/subcat']),
            ],
            'select2Options' => [
                'pluginOptions' => [
                    'width' => '100%',
                ]
            ],
        ]);
        ?>

    <?= ''// $form->field($model, 'flagId')->dropDownList($flagList, ['prompt' => 'Wybierz flagę']); ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Dodaj' : 'Zapisz', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
