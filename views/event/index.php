<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel app\models\EventSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Zdarzenia';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">Lista zdarzeń</h3>
        <div class="box-tools pull-right">
            <?= Html::a('Dodaj zdarzenie', ['create'], ['class' => 'btn btn-success']) ?>
        </div><!-- /.box-tools -->
    </div><!-- /.box-header -->
    <div class="box-body">
        <div class="event-index">
            <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
            <?=
            GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                    [
                        'attribute' => 'deviceId',
                        'label' => 'Numer seryjny urządzenia',
                        'value' => 'device.sn',
                    ],
                    [
                        'attribute' => 'flagId',
                        'label' => 'Flaga',
                        'value' => 'flag.name',
                        'filter' => ArrayHelper::map(app\models\Flag::find()->asArray()->all(), 'id', 'name'),
                    ],
                    'ip:ntext',
                    'created',
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'template' => '{view} {delete}'
                    ],
                ],
            ]);
            ?>
        </div>
    </div>
</div>
