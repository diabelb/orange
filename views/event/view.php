<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Event */

$this->title = 'Zdarzenie urządzenia: ' . $model->device->sn;
$this->params['breadcrumbs'][] = ['label' => 'Events', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">Przegląd zdarzenia</h3>
        <div class="box-tools pull-right">
            <?=
                Html::a('Usuń', ['delete', 'id' => $model->id], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => 'Are you sure you want to delete this item?',
                        'method' => 'post',
                    ],
                ])
                ?>
        </div><!-- /.box-tools -->
    </div><!-- /.box-header -->
    <div class="box-body">
        <div class="event-view">
            <p>
                
                
            </p>

            <?=
            DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'id',
                    [
                        'label' => 'Numer seryjny urządzenia',
                        'value' => $model->device->sn,
                    ],
                    [
                        'label' => 'Flaga',
                        'value' => $model->flag->name,
                    ],
                    'ip:ntext',
                    'created',
                ],
            ])
            ?>

        </div>
    </div>
</div>
