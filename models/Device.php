<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "device".
 *
 * @property integer $id
 * @property string $sn
 * @property string $created
 * @property string $modified
 * @property Event[] $events 
 */
class Device extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'device';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['sn'], 'required'],
            [['sn'], 'string'],
            ['sn', 'unique', 'targetAttribute' => ['sn'], 'message' => 'Urządzenie o takim numerze seryjnym już istnieje.'],
            [['created', 'modified'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'sn' => 'Numer seryjny',
            'created' => 'Utworzono',
            'modified' => 'Zmodyfikowano',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery 
     */
    public function getEvents() {
        return $this->hasMany(Event::className(), ['deviceId' => 'id']);
    }

}
