<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "flag".
 *
 * @property integer $id
 * @property string $name
 * @property string $ip
 * @property string $date
 */
class Flag extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'flag';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['name', 'ip'], 'required'],
            [['name', 'ip'], 'string'],
                //[['date'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'name' => 'Nazwa',
            'ip' => 'Adres IP',
            'date' => 'Data utworzenia',
        ];
    }

    public function getParents() {
        return $this->hasMany(Flag::className(), ['id' => 'parentId'])->viaTable('flag_flag', ['childId' => 'id']);
    }

    public function getChildren() {
        return $this->hasMany(Flag::className(), ['id' => 'childId'])->viaTable('flag_flag', ['parentId' => 'id']);
    }

    public function getHeadFlags() {
        $flagList = $this->find()->select(['id', 'name'])->all();
        $out = [];
        foreach ($flagList as $key => $value) {
            if (!$value->parents) {
                $out[] = $value;
            }
        }
        return $out;
    }

}
