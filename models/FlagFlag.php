<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "flag_flag".
 *
 * @property integer $id
 * @property integer $childId
 * @property integer $parentId
 *
 * @property Flag $child
 * @property Flag $parent
 */
class FlagFlag extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'flag_flag';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['childId', 'parentId'], 'required'],
            [['childId', 'parentId'], 'integer'],
            [['childId'], 'exist', 'skipOnError' => true, 'targetClass' => Flag::className(), 'targetAttribute' => ['childId' => 'id']],
            [['parentId'], 'exist', 'skipOnError' => true, 'targetClass' => Flag::className(), 'targetAttribute' => ['parentId' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'childId' => 'Dziecko',
            'parentId' => 'Rodzic',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChild()
    {
        return $this->hasOne(Flag::className(), ['id' => 'childId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParent()
    {
        return $this->hasOne(Flag::className(), ['id' => 'parentId']);
    }
}
