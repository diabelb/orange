<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "event".
 *
 * @property integer $id
 * @property integer $deviceId
 * @property integer $flagId
 * @property string $ip
 * @property string $created
 *
 * @property Flag $flag
 * @property Device $device
 */
class Event extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'event';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['deviceId', 'flagId', 'ip'], 'required'],
            [['deviceId', 'flagId'], 'integer'],
            [['ip'], 'string'],
            [['created'], 'safe'],
            [['flagId'], 'exist', 'skipOnError' => true, 'targetClass' => Flag::className(), 'targetAttribute' => ['flagId' => 'id']],
            [['deviceId'], 'exist', 'skipOnError' => true, 'targetClass' => Device::className(), 'targetAttribute' => ['deviceId' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'deviceId' => 'Numer seryjny urządzenia',
            'flagId' => 'Flaga',
            'ip' => 'Adres IP',
            'created' => 'Data utworzenia',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFlag()
    {
        return $this->hasOne(Flag::className(), ['id' => 'flagId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDevice()
    {
        return $this->hasOne(Device::className(), ['id' => 'deviceId']);
    }
}
