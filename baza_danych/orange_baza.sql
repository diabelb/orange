-- phpMyAdmin SQL Dump
-- version 4.6.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Czas generowania: 18 Lip 2016, 22:13
-- Wersja serwera: 5.6.31
-- Wersja PHP: 5.6.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `orange`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `device`
--

CREATE TABLE `device` (
  `id` int(10) UNSIGNED NOT NULL,
  `sn` text NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `device`
--

INSERT INTO `device` (`id`, `sn`, `created`, `modified`) VALUES
(1, 'fjs7ef898es0fhesfe0s', '2016-07-17 21:31:29', '2016-07-18 20:08:39'),
(2, 'rgdsgrger', '2016-07-17 21:36:15', '2016-07-18 21:20:23'),
(6, 'aaaaaaaaaaaa', '2016-07-18 21:34:42', '2016-07-18 22:01:29');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `event`
--

CREATE TABLE `event` (
  `id` int(10) UNSIGNED NOT NULL,
  `deviceId` int(10) UNSIGNED NOT NULL,
  `flagId` int(10) UNSIGNED NOT NULL,
  `ip` text NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `event`
--

INSERT INTO `event` (`id`, `deviceId`, `flagId`, `ip`, `created`) VALUES
(16, 2, 2, '192.168.56.1', '2016-07-18 20:39:53'),
(17, 2, 5, '192.168.56.1', '2016-07-18 20:50:52'),
(18, 2, 7, '192.168.56.1', '2016-07-18 20:54:43'),
(19, 2, 8, '192.168.56.1', '2016-07-18 21:20:23'),
(20, 6, 2, '192.168.56.1', '2016-07-18 21:51:15'),
(21, 6, 5, '192.168.56.1', '2016-07-18 22:00:46'),
(22, 6, 6, '192.168.56.1', '2016-07-18 22:01:04'),
(24, 6, 8, '192.168.56.1', '2016-07-18 22:02:52');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `flag`
--

CREATE TABLE `flag` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` text NOT NULL,
  `ip` text NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `flag`
--

INSERT INTO `flag` (`id`, `name`, `ip`, `date`) VALUES
(2, 'DEKOMPLETACJA', '192.168.56.1', '2016-07-17 16:18:16'),
(3, 'TESTOWANIE_USZKODZONY', '192.168.56.1', '2016-07-17 16:20:48'),
(4, 'PAKOWANIE_USZKODZONY', '192.168.56.1', '2016-07-17 16:23:01'),
(5, 'TESTOWANIE_SPRAWNY', '192.168.56.1', '2016-07-17 16:23:23'),
(6, 'CZYSZCZENIE', '192.168.56.1', '2016-07-17 16:23:32'),
(7, 'WYMIANA_OBUDOWY', '192.168.56.1', '2016-07-17 16:23:43'),
(8, 'PAKOWANIE', '192.168.56.1', '2016-07-17 16:23:56');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `flag_flag`
--

CREATE TABLE `flag_flag` (
  `id` int(10) UNSIGNED NOT NULL,
  `childId` int(10) UNSIGNED NOT NULL,
  `parentId` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `flag_flag`
--

INSERT INTO `flag_flag` (`id`, `childId`, `parentId`) VALUES
(1, 3, 2),
(3, 5, 2),
(4, 4, 3),
(5, 6, 5),
(6, 7, 5),
(7, 8, 7),
(9, 8, 6);

--
-- Indeksy dla zrzutów tabel
--

--
-- Indexes for table `device`
--
ALTER TABLE `device`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `event`
--
ALTER TABLE `event`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `flagId` (`flagId`),
  ADD KEY `deviceId` (`deviceId`),
  ADD KEY `id_2` (`id`);

--
-- Indexes for table `flag`
--
ALTER TABLE `flag`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_2` (`id`),
  ADD KEY `id` (`id`),
  ADD KEY `id_3` (`id`),
  ADD KEY `id_4` (`id`);

--
-- Indexes for table `flag_flag`
--
ALTER TABLE `flag_flag`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `child` (`childId`),
  ADD KEY `parent` (`parentId`),
  ADD KEY `id_2` (`id`),
  ADD KEY `childId` (`childId`),
  ADD KEY `parentId` (`parentId`),
  ADD KEY `id_3` (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT dla tabeli `device`
--
ALTER TABLE `device`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT dla tabeli `event`
--
ALTER TABLE `event`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT dla tabeli `flag`
--
ALTER TABLE `flag`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT dla tabeli `flag_flag`
--
ALTER TABLE `flag_flag`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- Ograniczenia dla zrzutów tabel
--

--
-- Ograniczenia dla tabeli `event`
--
ALTER TABLE `event`
  ADD CONSTRAINT `event_ibfk_1` FOREIGN KEY (`flagId`) REFERENCES `flag` (`id`),
  ADD CONSTRAINT `event_ibfk_2` FOREIGN KEY (`deviceId`) REFERENCES `device` (`id`);

--
-- Ograniczenia dla tabeli `flag_flag`
--
ALTER TABLE `flag_flag`
  ADD CONSTRAINT `flag_flag_ibfk_1` FOREIGN KEY (`childId`) REFERENCES `flag` (`id`),
  ADD CONSTRAINT `flag_flag_ibfk_2` FOREIGN KEY (`parentId`) REFERENCES `flag` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
